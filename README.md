# Cabecou & Clementine

Projet Hackathon de l'équipe 'Cabecou & Clémentine'

## Installation

* Installer node et npm
* Installer Angular CLI - `npm install -g @angular/cli` - [Documentation](https://angular.io/cli)

## Development server

Lancer `ng serve` pour démarrer le serveur. Aller à l'adresse `http://localhost:4200/`.
L'application se mettra à jour automatiquement.
